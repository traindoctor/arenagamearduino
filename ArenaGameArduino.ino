#define LED_DFLT_CONFLICT false
//Define whether the program should reset the color when the LED is turned off

//Default settings for LED and color on startup, modify only if required
#define STNG_DFLT_LED true
#define STNG_DFLT_COLOR false

#define STNG_DFLT_SERIAL false
//Default setting for serial monitor. 
//By default, program only outputs serial values to monitor when input is detected.

#define STNG_LOAD_ON_START true
//Load LED and color settings on start, this should be defined by the user. It will be enabled by default.

bool loadSTNG_1 = STNG_LOAD_ON_START;

//Custom settings for LED and color on startup, modify as you wish
#define STNG_CUSTOM_LED true
#define STNG_CUSTOM_COLOR false

#define STNG_CUSTOM_SERIAL true
//Custom setting for serial monitor. 
//Enabling this will cause program to constantly output values. Disabling this setting when loading

#define COMMON_ANODE

bool enableLED;
bool useColor;
bool cnstSerial;

int redPin = 4;
int greenPin = 3;
int bluePin = 2;

int redVal = 0;
int greenVal = 0;
int blueVal = 0;

int progState = 0; //0 = startup, 1 = loop

char dataLocal = 0;
char dataRemote = 0;

void setColor()
{
  #ifdef COMMON_ANODE
    redVal -= 255;
    greenVal -= 255;
    blueVal -= 255;
  #endif
  analogWrite(redPin, redVal);
  analogWrite(greenPin, greenVal);
  analogWrite(bluePin, blueVal);  
}

void setColor(int red, int green, int blue)
{
  #ifdef COMMON_ANODE
    redVal = 255 - red;
    greenVal = 255 - green;
    blueVal = 255 - blue;
  #endif
  analogWrite(redPin, redVal);
  analogWrite(greenPin, greenVal);
  analogWrite(bluePin, blueVal);  
}

int start()
{ //Set whether the program will load default or custom settings on startup.
  if(loadSTNG_1)
  {
    #define STNG_DEFAULT_ON_START true  //Use default settings or use custom settings, this should be defined by the user.
    bool loadSTNG_2 = STNG_DEFAULT_ON_START;
    
    if(loadSTNG_2)
    {
      #define STNG_CUSTOM_ON_START false //Automatic system for use of custom or default settings, change at user's discretion.
      bool loadSTNG_3 = STNG_CUSTOM_ON_START;
      Serial.print("[START]: Settings loaded on start. Manual input of critical and non-critical settings not required.\n");
      Serial.print("[START]: The creator of this program is not responsible for any damage caused by user tweaking of citical default settings.\n");
      return 1;
    } 
    
    if(!loadSTNG_2)
    {
      #define STNG_CUSTOM_ON_START true
      bool loadSTNG_3 = STNG_CUSTOM_ON_START;
      Serial.print("[START]: Settings loaded on start. Manual input of critical and non-critical settings not required.\n");
      Serial.print("[START]: The creator of this program is not responsible for any damage caused by user tweaking of citical default or custom settings.\n");
      return 2;
    }
  }

  if(!loadSTNG_1)
  {
    Serial.print("[START]: Settings not loaded on start. Manual input of critical and non-critical settings required.\n");
    Serial.print("[START]: Failue to input settings in the correct order could lead to damage of electronic components.\n");
    Serial.print("[START]: The creator of this program is not responsible for any damage caused by user tweaking.\n");
    return 0;
  }
}

void load(int value)
{
  switch(value)
  {
    case 0:
      switch(progState)
      {
        case 0:
        Serial.print("[LOAD]: NO SETTINGS LOADED ON START\n");
        progState = 1;
        break;

        case 1:
        Serial.print("[LOAD]: NO SETTINGS LOADED IN LOOP\n");
        progState = 1;
        break;

        default: //Will remain default until further values are defined
        Serial.print("[LOAD]: NO SETTINGS LOADED, VALUE 0: PROGSTATE: DEFAULT\n");
        progState = 1;
        break;           
      }
    break;

    case 1:
      switch(progState)
      {
        case 0:
          enableLED = STNG_DFLT_LED;
          useColor = STNG_DFLT_COLOR;
          cnstSerial = STNG_DFLT_SERIAL;
          Serial.print("[LOAD]: START SETTINGS LOADED\n");
          Serial.print("[LOAD]: STNG: enableLED = STNG_DFLT_LED\n");
          Serial.print("[LOAD]: STNG: useColor = STNG_DFLT_COLOR\n");
          Serial.print("[LOAD]: STNG: cnstSerial = STNG_DFLT_SERIAL\n");
          progState = 1;
        break;

        // case 1:
          // enableLED = STNG_DFLT_LED;
          // useColor = STNG_DFLT_COLOR;
          // cnstSerial = STNG_DFLT_SERIAL;
          // Serial.print("[LOAD]: START SETTINGS LOADED\n");
          // Serial.print("[LOAD]: STNG: enableLED = STNG_DFLT_LED\n");
          // Serial.print("[LOAD]: STNG: useColor = STNG_DFLT_COLOR\n");
          // Serial.print("[LOAD]: STNG: cnstSerial = STNG_DFLT_SERIAL\n");
          // progState = 1;
        // break;

        default: //Will remain default until further values are defined
        Serial.print("[LOAD]: NO SETTINGS LOADED, VALUE 1: PROGSTATE: DEFAULT\n");
        break;           
      }
    break;

    case 2:
      switch(progState)
      {
        case 0:
          enableLED = STNG_CUSTOM_LED;
          useColor = STNG_CUSTOM_COLOR;
          cnstSerial = STNG_CUSTOM_SERIAL;
          Serial.print("[LOAD]: START SETTINGS LOADED\n");
          Serial.print("[LOAD]: STNG: enableLED = STNG_CUSTOM_LED\n");
          Serial.print("[LOAD]: STNG: useColor = STNG_CUSTOM_COLOR\n");
          Serial.print("[LOAD]: STNG: cnstSerial = STNG_CUSTOM_SERIAL\n");
          progState = 1;
        break;

        // case 1:
          // enableLED = STNG_CUSTOM_LED;
          // useColor = STNG_CUSTOM_COLOR;
          // cnstSerial = STNG_CUSTOM_SERIAL;
          // Serial.print("[LOAD]: START SETTINGS LOADED\n");
          // Serial.print("[LOAD]: STNG: enableLED = STNG_CUSTOM_LED\n");
          // Serial.print("[LOAD]: STNG: useColor = STNG_CUSTOM_COLOR\n");
          // Serial.print("[LOAD]: STNG: cnstSerial = STNG_CUSTOM_SERIAL\n");
          // progState = 1;
        // break;

        default: //Will remain default until further values are defined
        Serial.print("[LOAD]: NO SETTINGS LOADED , VALUE 2: PROGSTATE: DEFAULT\n");
        break;           
      }
    break;
  }
}

void dataRead(char input)
{
   switch(input)
   {
          case '1':
            if(enableLED && useColor)
            {
              redVal = 255, greenVal = 0, blueVal = 0;
              setColor();
            }
            if(enableLED && !useColor) digitalWrite(LED_BUILTIN, HIGH);
          break;

          case '2':
            if(enableLED && useColor)
            {
              redVal = 0, greenVal = 255, blueVal = 0;
              setColor();         
            } 
          break;

          case '3':
            if(enableLED && useColor)
            {
              redVal = 0, greenVal = 0, blueVal = 255;
              setColor();         
            } 
          break;

          case '4':
            if(enableLED && useColor)
            {
              redVal = 255, greenVal = 255, blueVal = 0;
               setColor();         
            } 
          break;

          case '5':
            if(enableLED && useColor)
            {
              redVal = 0, greenVal = 255, blueVal = 255;
              setColor();         
            } 
          break;

          case '6':
            if(enableLED && useColor)
            {
              redVal = 255, greenVal = 255, blueVal = 255;
              setColor();         
            }
          break;

          case '7':
            if(enableLED && useColor)
            {
              redVal = 80, greenVal = 0, blueVal = 80;
              setColor();         
            } 
          break;

          case '8':
            if(enableLED && useColor)
            {
              redVal = 80, greenVal = 80, blueVal = 0;
              setColor();         
            } 
          break;

          case '9':
            if(enableLED && useColor)
            {
              redVal = 0, greenVal = 80, blueVal = 80;
              setColor();         
            } 
          break;

          case '0':
            if(enableLED && useColor)
            {
              redVal = 80, greenVal = 80, blueVal = 80;
              setColor();         
            } 
            if(enableLED && !useColor) digitalWrite(LED_BUILTIN, LOW);
          break;

          case 'E':
            enableLED = true; 
          break;

          case 'D':
            enableLED = false;
          break;

          case 'Y':
            useColor = true; 
          break;

          case 'N':
            useColor = false; 
          break;

          default: //Currently unused due to a bug with how values are read
          //Serial.print("Serial value: "); Serial.print(data); Serial.print("\n");
          break;
   }
}

void read()
{
  while(Serial.available())
  {
    if(cnstSerial)
    {
      while(Serial.read() || Serial3.read())
      {
        dataLocal = Serial.read(); //Read the incoming data & store into data
        Serial.print("[SERIAL]: LOCAL INPUT: "); Serial.print(dataLocal); Serial.print("\n"); //Print value inside data in serial monitor

        dataRead(dataLocal);
           
        if(!enableLED && useColor) redVal = 0, greenVal = 0, blueVal = 0;    

        if(enableLED && useColor) setColor();
              
        if(enableLED && !useColor) digitalWrite(LED_BUILTIN, HIGH);

        if(!enableLED && !useColor) digitalWrite(LED_BUILTIN, LOW); 
      }
    }

    if(!cnstSerial)
    {
      while(Serial.read() > 0)
      {
        dataLocal = Serial.read(); //Read the incoming data & store into data
        Serial.print("[SERIAL]: LOCAL INPUT: "); Serial.print(dataRemote); Serial.print("\n"); //Print value inside data in serial monitor

        dataRead(dataLocal);
           
        if(!enableLED && useColor) redVal = 0, greenVal = 0, blueVal = 0;    

        if(enableLED && useColor) setColor();
              
        if(enableLED && !useColor) digitalWrite(LED_BUILTIN, HIGH);

        if(!enableLED && !useColor) digitalWrite(LED_BUILTIN, LOW); 
      }
    }
  }

  while(Serial3.available())
  {
    if(cnstSerial)
    {
      while(Serial3.read())
      {
        dataRemote = Serial3.read(); //Read the incoming data & store into data
        Serial3.print("[SERIAL]: REMOTE INPUT: "); Serial3.print(dataRemote); Serial3.print("\n"); //Print value inside data in serial monitor

        dataRead(dataRemote);
           
        if(!enableLED && useColor) redVal = 0, greenVal = 0, blueVal = 0;

        if(enableLED && useColor) setColor();
              
        if(enableLED && !useColor) digitalWrite(LED_BUILTIN, HIGH);

        if(!enableLED && !useColor) digitalWrite(LED_BUILTIN, LOW);
      }
    }

    if(!cnstSerial)
    {
      while(Serial3.read() > 0)
      {
        dataRemote = Serial3.read(); //Read the incoming data & store into data
        Serial3.print("[SERIAL]: REMOTE INPUT: "); Serial3.print(dataRemote); Serial3.print("\n"); //Print value inside data in serial monitor

        dataRead(dataRemote);
           
        if(!enableLED && useColor) redVal = 0, greenVal = 0, blueVal = 0;

        if(enableLED && useColor) setColor();
              
        if(enableLED && !useColor) digitalWrite(LED_BUILTIN, HIGH);

        if(!enableLED && !useColor) digitalWrite(LED_BUILTIN, LOW);
      }
    }
  }
}

void setup()
{
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT); 
  //Setup for the output pins which the arduino uses to change the color of the LED

  pinMode(LED_BUILTIN, OUTPUT);
  //Setup for the builtin LED on the arduino for testing input from the program
  
  // Open serial communications and wait for port to open:
  Serial.begin(57600);
  while(!Serial){;} // wait for serial port to connect. Needed for native USB port only
}

void loop() 
{
  if(progState == 0) 
  {
    load(start());
  }
  read(); 
}